Установка RubyOnRails в Ubuntu 14.04

#Обновление системы
sudo apt-get update

# установка git и других библиотек
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties


sudo apt-get install libgdbm-dev libncurses5-dev automake libtool bison libffi-dev

# установка RVM
curl -L https://get.rvm.io | bash -s stable
source ~/.rvm/scripts/rvm
echo "source ~/.rvm/scripts/rvm" >> ~/.bashrc
rvm install 2.1.5
rvm use 2.1.5 --default
ruby -v

# Отключаем установку документации с гемами
echo "gem: --no-ri --no-rdoc" > ~/.gemrc